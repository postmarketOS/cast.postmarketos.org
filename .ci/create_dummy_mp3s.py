#!/usr/bin/env python3
# Copyright 2023 Laszlo Molnar
# SPDX-License-Identifier: AGPL-3.0-or-later

'''Script that creates empty mp3 files so that freeze (build of static content)
will not convert all opus files to mp3 on ci.'''

from os import path, listdir


if __name__ == '__main__':
    mp3_dir = path.join('static', 'audio')
    opus_filenames = [path.splitext(f)[0] for f in listdir(mp3_dir)
                      if f.endswith('.opus')]
    for filename in sorted(opus_filenames)[:-1]:  # note, up to last item
        mp3_path = path.join(mp3_dir, f'{filename}.mp3')
        open(mp3_path, 'w').close()  # creates empty mp3 file
