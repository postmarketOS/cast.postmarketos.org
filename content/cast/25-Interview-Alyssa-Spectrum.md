date: 2022-11-20
title: "#25 INTERVIEW: Alyssa Ross (of Spectrum Fame)"
length: "1:11:07"
timestamps:
  - "01:20 A long trip"
  - "08:03 What is Spectrum?"
  - "17:41 Portals & Flatpak"
  - "32:55 Spectrum's relation to NixOS"
  - "39:53 Linux-firmware packaging"
  - "42:57 pmOS default experience"
  - "52:00 How many people use pmOS as their main phone OS?"
  - "55:10 Phone calls with pmOS"
  - "58:33 How to contribute to Spectrum?"
  - "65:06 Making of"
---

What happens if the author of an upcoming security focused desktop OS, who is
at the same time also a postmarketOS user, comes on our podcast? A lot of
interesting questions being asked and answered from both sides for sure!

Featuring @ollieparanoid, @qyliss, @calebccff, @MartijnBraam (in order of
appearance).

Referenced in this episode:

* [Spectrum — a step towards *usable* secure computing](https://spectrum-os.org/)
    * Using packages from [NixOS](https://nixos.org/)
    * Using [virtio-fs](https://virtio-fs.gitlab.io/)
    * Inspired by [Qubes OS](https://www.qubes-os.org/)
    * [Participating in Spectrum](https://spectrum-os.org/participating.html)
    * [Alyssa accepts donations](https://alyssa.is/about/)

* [pma#1596](https://gitlab.com/postmarketOS/pmaports/-/issues/1596):
  concept for privilege separation in postmarketOS

* [pmOS principles, user facing](https://wiki.postmarketos.org/wiki/About_postmarketOS#User_facing)

* [pma#1133](https://gitlab.com/postmarketOS/pmaports/-/issues/1133):
  enable zram swap by default for some (or all?) devices

* How many users do we have?
  [Martijn was curious as well](https://fosstodon.org/@martijnbraam/109375865784863201)

* [VDO.ninja](https://vdo.ninja/)
  ([source](https://github.com/steveseguin/vdo.ninja))

<small>Editing by: @MartijnBraam,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
