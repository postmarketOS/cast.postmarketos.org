date: 2024-01-28
title: "#37 Clayton goes full-time, v23.12, smart speakers, musl locales"
length: "31:24"
timestamps:
  - "00:00 Clayton goes full-time"
  - "04:12 v23.12"
  - "11:33 FOSDEM 2024"
  - "14:10 How to become a Trusted Contributor"
  - "20:39 Amazon Echo Dot 2nd gen"
  - "24:22 New PinePhone (Pro) Maintainers"
  - "25:27 musl locales"
  - "27:30 mrtest"
---
Clayton totally surprises us for real, with his announcement of working
full-time on postmarketOS from now on. Furthermore we are super hyped
about the upcoming FOSDEM 2024 (as we release this episode, only a week away!)
and talk among other topics about how *you* could become a Trusted Contributor.

Featuring @craftyguy, @ollieparanoid, @calebccff, @pabloyoyoista (in order of
appearance).

To support postmarketOS financially, especially now with Clayton working
full-time:

* [Check out the shiny new Open Collective page](https://opencollective.com/postmarketos)
* [Read the announcement: postmarketOS needs your help!](https://postmarketos.org/blog/2023/12/14/pmos-on-oc/)

Referenced in this episode:

* [Clayton: "Today I start my new job, …](https://freeradical.zone/@craftyguy/111506290148662542)
* [v23.12: The One We Asked The Community To Name](https://postmarketos.org/blog/2023/12/18/v23.12-release/)
* FOSDEM 2024:
    * [FOSS on Mobile Devices devroom](https://fosdem.org/2024/schedule/track/foss-on-mobile-devices/)
      (full track listing is announced!)
    * [Stands](https://fosdem.org/2024/stands/), we'll be at Linux on Mobile
* [How to become a Trusted Contributor](https://postmarketos.org/blog/2023/12/03/how-to-become-tc/)
    * [Part 2](https://postmarketos.org/blog/2024/01/28/how-to-become-tc2/)
* [Amazon Echo Dot 2nd gen](https://wiki.postmarketos.org/wiki/Amazon_Echo_Dot_2nd_gen_(amazon-biscuit))
  (thanks to Ben Westover!)
    * [iFixit guide](https://www.ifixit.com/Guide/Amazon+Echo+Dot+2nd+Generation+4-in-1+WiFi-Bluetooth-FM-GPS+Replacement/100835)
    * [Rooting the Amazon Echo Dot](https://dragon863.github.io/blog/echoroot.html)
    * {{MR|4532|pmaports}}: amazon-biscuit: new device
    * {{MR|49|boot-deploy}}: add\_mtk\_header: Replace hardcoded MediaTek header labels with deviceinfo variables
    * [pmbootstrap: parse.bootimg: Separate kernel and ramdisk MediaTek headers](https://gitlab.com/postmarketOS/pmbootstrap/-/commit/c10a3ce73b56d229ddca2d11bc0e272774cd7a2d)
* New maintainers for PINE64 devices ({{MR|4529|pmaports}}, {{MR|4736|pmaports}})
    * Thanks to Jan Jasper de Kroon for taking over PinePhone Pro!
    * Thanks to Arnav Singh for taking over PinePhone!
    * And not in the recorded episode since it just happened a week ago, but
      also huge thanks to Quade Curry for taking over the PineBook Pro!
* musl: [setlocale() again](https://www.openwall.com/lists/musl/2023/08/10/3)
  mailing list thread
* [mrtest: allow to select multiple packages and use ranges](https://gitlab.com/postmarketOS/mrhlpr/-/merge_requests/23)
  (thanks to Petr Hodina!)

<small>Editing by: @ollieparanoid,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
