date: 2021-09-20
title: "#9 PBP, RP64, ModemManager, Phosh customization, Principles"
length: "45:10"
timestamps:
  - "00:32 New devices in community"
  - "07:54 ModemManager"
  - "21:33 postmarketOS-tweaks"
  - "29:55 colorized pmbootstrap"
  - "32:55 ollie went to the movies"
---

We share the fun times we had debugging ModemManager call and SMS handling
with the PinePhone. The bunch of news also includes how the PineBookPro and
RockPro64 leveled up from testing to community and the black magic that had to
be performed to get custom lockscreen and app menu backgrounds in Phosh with
postmarketOS tweaks.

Like always, write feedback with `#postmarketOSpodcast` to our
[Mastodon](https://fosstodon.org/@postmarketOS).

Featuring @craftyguy, @PureTryOut, @MartijnBraam, @minlexx, @dylanvanassche,
@ollieparanoid (in order of appearance).

Referenced in this episode:

* More PINE64 devices moved from up "testing" to "community" categories
* New devices in community category
    * [device categorization](https://wiki.postmarketos.org/wiki/Device_categorization)
    * [Pinebook Pro](https://wiki.postmarketos.org/wiki/PINE64_Pinebook_Pro_(pine64-pinebookpro))
    * [RockPro64](https://wiki.postmarketos.org/wiki/PINE64_RockPro64_(pine64-rockpro64))

* [Hourly monitoring](https://gitlab.com/postmarketOS/monitoring/-/pipelines)


* ModemManager: QMI indications vs AT URCs
    * upstream discussion:
     [ModemManager#356](https://gitlab.freedesktop.org/mobile-broadband/ModemManager/-/issues/356)
    * ModemManager release 1.18 is out!
    * Meanwhile Dylan already implemented the fallback to AT URCs for calls
      too:
      [ModemManager!605](https://gitlab.freedesktop.org/mobile-broadband/ModemManager/-/merge_requests/605)
    * [libqmi](https://www.freedesktop.org/wiki/Software/libqmi/)
    * [Create usbmon capture and look at it with WiresharkQMIDissector](https://wiki.postmarketos.org/wiki/Create_usbmon_capture)

* [Biktor's pinephone_modem_sdk](https://github.com/Biktorgj/pinephone_modem_sdk)

* Colorized pmbootstrap:
    * [pmb!2090](https://gitlab.com/postmarketOS/pmbootstrap/-/merge_requests/2090)
    * samsung-i9300 warning regarding hardware failure was added in
      [pma!2229](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2229)
      with follow up in
      [pma#1115](https://gitlab.com/postmarketOS/pmaports/-/issues/1115)

* More Phosh customization with
  [postmarketOS tweaks](https://gitlab.com/postmarketOS/postmarketos-tweaks)
  0.7.x
    * @pojntfx shows the release in action:
      [twitter](https://twitter.com/pojntfx/status/1421820675317248003),
      [nitter](https://nitter.net/pojntfx/status/1421820675317248003)
    * [v21.06.1](https://postmarketos.org/blog/2021/08/04/v21.06.1-release/)
      backported this to stable

* ollie went to the movies
    * Alles ist eins. Außer der 0.
        * [CCC announcement](https://www.ccc.de/en/updates/2021/allesisteins) (german)
        * [epic movie page](https://allesisteins.film/) (german)
    * [CCC on hacker ethics](https://www.ccc.de/en/hackerethik)
    * [Where is @postmarketOS?](https://postmarketos.org/chats-and-social-media/)
    * [About postmarketOS](https://wiki.postmarketos.org/wiki/About_postmarketOS)
      featuring our principles

<small>Editing by: @MartijnBraam,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
