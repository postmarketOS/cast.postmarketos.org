date: 2022-06-28
title: "#19 apk fix, qbootctl, Phosh 0.20, iPhones and more Apples"
length: "29:16"
timestamps:
  - "01:03 release upgrade / apk fix"
  - "05:27 Plasma Mobile + GTK keyboard fix"
  - "07:34 himitsu-keyring"
  - "09:33 qbootctl"
  - "14:45 Phosh 0.20"
  - "17:57 GNOME Shell on mobile"
  - "19:12 iPhones and more Apples"
  - "23:50 Fix for modem disappearing with Biktor's FW"

---
Linux on A7-A8X gets us pretty excited among lots of other topics. What are you
reading this for? It's a podcast, hit play already! Featuring @ollieparanoid,
@MartijnBraam, @z3ntu, @PureTryOut (in order of appearance).

Referenced in this episode:

* postmarketos-release-upgrade
    * [git repo](https://gitlab.com/postmarketOS/postmarketos-release-upgrade)
    * Wiki: [Upgrade to a newer postmarketOS release](https://wiki.postmarketos.org/wiki/Upgrade_to_a_newer_postmarketOS_release)
    * LINMOB.net: Upgrading to postmarketOS 22.06 (Phosh) on PinePhone
      ([TilVids](https://tilvids.com/w/ohK7QfPp5Ej8Q1Y9GucLCR),
      [YouTube](https://www.youtube.com/watch?v=zKZj9aQmAjA))
    * Should we just ignore small packaging errors and run 'apk fix'
      afterwards? ([relup#5](https://gitlab.com/postmarketOS/postmarketos-release-upgrade/-/issues/5))
    * [v22.06 is out!](https://postmarketos.org/blog/2022/06/12/v22.06-release/)

* Plasma Mobile + GTK keyboard fix
    * [pma#1050](https://gitlab.com/postmarketOS/pmaports/-/issues/1050)

* himitsu-keyring
    * [git repo](https://git.sr.ht/~martijnbraam/keyring)
    * packaged in Alpine: `apk add himitsu-keyring`
    * Drew DeVault:
      [Introducing the Himitsu keyring & password manager for Unix](https://drewdevault.com/2022/06/20/Himitsu.html)

* qbootctl
    * [git repo](https://gitlab.com/sdm845-mainline/qbootctl)
    * Install qbootctl on A/B sdm845 devices
      ([pma!3190](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3190))

* Phosh 0.20
    * [v0.20.0_beta1](https://gitlab.gnome.org/World/Phosh/phosh/-/tags/v0.20.0_beta1)
      and [beta2](https://gitlab.gnome.org/World/Phosh/phosh/-/tags/v0.20.0_beta2)
    * Martijn: [Phosh 0.20 Beta 1 on a PinePhone](https://www.youtube.com/watch?v=n7dBqWlzt-A)

* GNOME Shell on mobile
    * GNOME.org: [Towards GNOME Shell on mobile](https://blogs.gnome.org/shell-dev/2022/05/30/towards-gnome-shell-on-mobile/)
    * [Caleb giving it a try on OnePlus 6](https://nitter.net/calebccff/status/1531737951612411904)
      and does a [side-by-side with Phosh](https://nitter.net/calebccff/status/1531743013797502977)
    * Martijn: [Gnome stack performance on the PinePhone and Samsung Galaxy SII](https://fosstodon.org/@martijnbraam/108398708284012607)

* iPhones and more Apples
    * [Initial announcement post by Konrad and @quaack723](https://nitter.net/konradybcio/status/1531963130934329344)
    * Konrad Dybcio: [Linux on A7-A8X - How we got Linux on the iPhone, iPad and other iDevices](https://konradybcio.pl/linuxona7/)
      (the definitive blog post to read about this, also maybe [buy Konrad a cup of tea](https://konradybcio.pl/donate/) ☕)
    * @quaack723: [new Matrix room for Apple mainline porting](https://nitter.net/quaack723/status/1534443214983266304)
    * [linux-apple-resources](https://github.com/SoMainline/linux-apple-resources)
    * Konrad's [call to extend](https://nitter.ca/konradybcio/status/1532148620191059969)
      [adt_collection](https://github.com/SoMainline/adt_collection)
      "not only a7-a8x, every iDevice is welcome! (did I mention homepod runs on a8)"
    * @riscv64: [XFCE4 on iPad Pro 9.7 Inch (Wi-Fi)](https://nitter.net/riscv64/status/1535971017616953345)
    * @dedbeddedbed: [linux on apple tv 4](https://nitter.net/dedbeddedbed/status/1538272127924809730)
    * arstechnica: [Have an old iPad lying around? You might be able to make it run Linux soon](https://arstechnica.com/gadgets/2022/06/developers-get-linux-up-and-running-on-old-ipad-air-2-hardware/)

* Fix for modem disappearing with @Biktorgj's FW
    * Make open firmware identifyable in udev
      ([#122](https://github.com/Biktorgj/pinephone_modem_sdk/issues/112))

<small>Editing by: @ollieparanoid,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
