date: 2022-01-30
title: "#13 EG25-G + fwupd, unudhcpd, mainline.space, OP9, iwd"
length: "56:24"
timestamps:
  - "00:30 EG25-G + fwupd"
  - "17:51 Cross-distro PinePhone kernel + bootloader"
  - "23:46 unudhcpd"
  - "34:01 OnePlus 9 Mainline"
  - "39:10 mainline.space"
  - "46:20 iwd"
  - "53:49 FOSDEM 2022"
---
An easy way to upgrade the PinePhone's modem to Biktor's firmware is quite the
puzzle to solve. Dylan has been finding and patching one piece after another,
and elaborates what the whole picture with fwupd looks like. Besides that, we
get into a lot of mainlining related topics and even answer one listener
question.

Featuring  @craftyguy, @MartijnBraam, @calebccff, @dylanvanassche, @PureTryOut,
@ollieparanoid (in order of appearance).

Referenced in this episode:

* EG25-G + fwupd
    * Dylan: [Upgrading your PinePhone modem easily](https://dylanvanassche.be/blog/2022/pinephone-modem-upgrade/)
    * Biktor: [pinephone_modem_sdk](https://github.com/Biktorgj/pinephone_modem_sdk), [quectel_lk](https://github.com/Biktorgj/quectel_lk)
    * nns: [This blog is now hosted on a GPS/LTE modem](https://nns.ee/blog/2021/04/01/modem-blog.html)
    * [fwupd](https://github.com/fwupd/fwupd)
    * [LVFS](https://fwupd.org/)

* Cross-distro PinePhone kernel + bootloader efforts
    * [shared kernel repo](https://gitlab.com/pine64-org/linux)

* unudhcpd
    * [gitlab project](https://gitlab.com/postmarketOS/unudhcpd)
    * [!6](https://gitlab.com/postmarketOS/unudhcpd/-/merge_requests/6): glibc fix

* OnePlus 9 / 9 Pro Mainline (SoC: SM8350)
    * [wiki page](https://wiki.postmarketos.org/wiki/OnePlus_9_%26_9_Pro_(oneplus-lemonade_%26_oneplus-lemonadep))
    * [pma!2459](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2459): initial MR

* mainline.space
    * [https://mainline.space/](https://mainline.space/)
    * [https://not.mainline.space/](https://not.mainline.space/)

* iwd
    * [Homepage/wiki](https://iwd.wiki.kernel.org/)
    * Talk at Embedded Linux Conference: [The New Wi-Fi Experience for Linux](https://www.youtube.com/watch?v=QIqT2obSPDk)
    * Action plan in [pma#1379](https://gitlab.com/postmarketOS/pmaports/-/issues/1379):
      Roll out iwd instead of wpa_supplicant in pmOS
    * [pmb!2133](https://gitlab.com/postmarketOS/pmbootstrap/-/merge_requests/2133)
      implemented the kconfig check
    * Related comment in [pma#973](https://gitlab.com/postmarketOS/pmaports/-/issues/973#note_805056452):
      Pinephone: reduce WiFi reconnection time after deep sleep

* FOSDEM 2022 is next weekend - 5th and 6th of February!
    * [FOSS on Mobile Devices devroom](https://fosdem.org/2022/schedule/track/foss_on_mobile_devices/)
      (track schedule)

<small>Editing by: @MartijnBraam,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
