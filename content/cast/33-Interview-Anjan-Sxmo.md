date: 2023-08-02
title: "#33 INTERVIEW: Anjan (of Sxmo Fame)"
length: "25:27"
timestamps:
  - "00:10 Welcome"
  - "00:30 What's FOSSY?"
  - "01:35 Why'd you go?"
  - "02:32 Anjan's talk"
  - "04:28 Swapping OP6 & Poco F1"
  - "06:46 Reception"
  - "08:25 How'd you get into Sxmo?"
  - "14:37 How to get involved?"
  - "16:11 git send-email"
  - "18:33 Plans for Sxmo"
  - "20:22 Onboarding in pmOS"
  - "22:15 Shout out"
  - "23:20 Wrapping up"
---
This time we did a mix of an interview and an event report with Anjan from
[Sxmo](https://sxmo.org) who just was at [FOSSY 2023](https://2023.fossy.us/)
in Portland, OR, USA. Sxmo is a minimalist environment for Linux mobile
devices, allowing you to run a tiling window manager on your smartphone.

Featuring @ollieparanoid, @anjan (in order of appearance).

Referenced in this episode:

* Anjan's talk:
  [Thoughts after daily driving postmarketOS for 3 years](https://2023.fossy.us/schedule/presentation/81/)
    * Video is not up yet, but Anjan said he'll post it on his
      [Mastodon](https://fosstodon.org/@anjan@pleroma.debian.social/) when it is
* [Software Freedom Conservancy](https://sfconservancy.org/)
* [Contribute to Sxmo](https://sxmo.org/contribute)
* [sxmo-utils: sxmo_modem.sh: retry if failed to delete sms](https://lists.sr.ht/~mil/sxmo-devel/patches/41607)
* WIP Sxmo lockscreen: [peanutbutter](https://git.sr.ht/~anjan/peanutbutter)

Related episodes:

* Sxmo: Interviews with @proycon (E05) and Miles Alan (E15)
* SDM845: Interview with @calebccff (E08) / pretty much any episode with Caleb

<small>Editing by: @MartijnBraam,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
