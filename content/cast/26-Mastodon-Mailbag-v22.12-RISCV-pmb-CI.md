date: 2022-12-12
title: "#26 Mastodon, Mailbag, v22.12, RISC-V, pmbootstrap CI"
length: "50:56"
timestamps:
- "01:06 Mastodon"
- "07:00 Survey Results"
- "10:58 Mailbag"
- "25:40 Megapixels"
- "28:18 Waydroid"
- "29:35 Mediatek Mainlining"
- "33:32 SP3"
- "34:00 RISC-V"
- "36:10 v22.12"
- "45:05 pmbootstrap ci"
---

Like every other podcast in your feed we give our two cents on what's been
happening in the Fediverse, like why it's really great to see so many free
software related projects and people moving over from that bird site. And that
one should seriously consider supporting the admins of the instance they are
using with a monthly donation or something. Besides that, we share some news on
soon-to-be-released v22.12 such as call audio support for OP6 and other cool
stuff that has been happening.

Featuring @MartijnBraam, @calebccff, @ollieparanoid (in order of appearance).

Referenced in this episode:

* Mastodon:
    * [Fosstodon: More Upgrades!](https://hub.fosstodon.org/more-upgrades-twitter-storm/)
    * Co-author of the ActivityPub spec explaining the ecosystem and its
      history in more detail in a great podcast episode:
      [FOSS and Crafts 53: Fediverse reflections while the bird burns](https://fossandcrafts.org/episodes/053-fediverse-reflections-while-the-bird-burns.html)

* Poll: [do you daily drive a postmarketOS phone?](https://fosstodon.org/@martijnbraam/109375865784863201)

* [Late Night Linux 205](https://latenightlinux.com/late-night-linux-episode-205/)
  where Joe tried out pmOS on a Samsung Galaxy S III

* [Lemmy comment](https://lemmy.ml/post/611305?scrollToComments=true)
  describing LXC/LXD based Panixticon virtualization project

* [Megapixels 1.6.0](https://gitlab.com/postmarketOS/megapixels/-/tags/1.6.0)

* [Waydroid: pmOS image deleted, requires user intervention](https://postmarketos.org/edge/2022/10/14/waydroid/)

* [Jami's amazing Volla Phone (original and 22) Mediatek mainlining](https://fosstodon.org/@deathmist/109366195866974426)

* pmOS wiki: [PinePhone U-Boot Upgrade](https://wiki.postmarketos.org/wiki/PinePhone_U-Boot_Upgrade)

* pmOS wiki: [pmbootstrap CI](https://wiki.postmarketos.org/wiki/Pmbootstrap_CI)

<small>Editing by: @ollieparanoid,
Music by: [The Passion HiFi](http://www.thepassionhifi.com), @ollieparanoid</small>
