date: 2024-08-14
title: "#42 INTERVIEW: verdre (of GNOME Shell on Mobile Fame)"
length: "41:12"
timestamps:
  - "00:00 driving up"
  - "01:23 origin story"
  - "03:36 performance optimizations"
  - "05:29 release 46-mobile-0"
  - "08:24 gestures"
  - "10:52 hanging in bars"
  - "12:54 airpods vuln"
  - "21:24 upstreaming"
  - "24:35 community"
  - "25:32 aliendalvik"
  - "30:15 how to contribute"
  - "31:12 smooth animations"
  - "31:41 extensions"
  - "32:55 mini guadec in berlin"
  - "34:13 questions for us"
  - "37:11 shout out"
  - "37:28 wrapping up"

---
Jonas tells us some crazy stories about how together with Tobias he started
GNOME Shell on Mobile, how he found quite the important vulnerability in
Apple's AirPods, running Aliendalvik from Sailfish OS and more!

Featuring @craftyguy, @verdre and @ollieparanoid (in order of appearance).

GNOME Shell on Mobile:

* [Matrix room](https://matrix.to/#/#mobile:gnome.org)
* [gnome-shell-mobile.git](https://gitlab.gnome.org/verdre/gnome-shell-mobile)
* Blog post: [Towards GNOME Shell on mobile](https://blogs.gnome.org/shell-dev/2022/05/30/towards-gnome-shell-on-mobile/)
* Blog post: [GNOME Shell on mobile: An update](https://blogs.gnome.org/shell-dev/2022/09/09/gnome-shell-on-mobile-an-update/)
* [verdre: "Alright, I’ve finally done a new #GNOME  Shell Mobile release! Time for a little thread about the updates..."](https://mastodon.social/@verdre/112791089339958634)
  &mdash; *about the latest GNOME 46 based version*
* [AdrianVovk: "Just got GNOME Mobile 46 built and running in @postmarketOS on my OP6, here at #GUADEC2024 in Denver, so the folks in Berlin aren't the only ones that can try it out! ..."](https://fosstodon.org/@AdrianVovk/112827126580786458)
  &mdash; *with a nice photo!*

AirPods:

* Blog post: [Do a firmware update for your AirPods – now](https://blogs.gnome.org/jdressler/2024/06/26/do-a-firmware-update-for-your-airpods-now/)
* [Apple's security advisory](https://support.apple.com/en-us/HT214111)
* [MagicPairing: Apple’s Take on Securing Bluetooth Peripherals](https://arxiv.org/pdf/2005.07255)

Aliendalvik:

* Blog post: [A dive into Jolla AppSupport](https://blogs.gnome.org/jdressler/2023/12/20/a-dive-into-jolla-appsupport/)

(Berlin Mini) GUADEC 2024:

* Blog post: [Save the Date: Berlin Mini GUADEC 2024](https://blogs.gnome.org/tbernard/2024/01/29/save-the-date-berlin-mini-guadec-2024/)
* Blog post: [GUADEC 2024 in Denver, Colorado](https://foundation.gnome.org/2023/12/20/guadec-2024-in-denver-colorado/)
* Videos: [Berlin Mini GUADEC](https://www.youtube.com/channel/UCMUOxTP2MLsNDjSEuIPllww)
* Videos: [GNOME](https://www.youtube.com/@GNOMEDesktop) (has full day streams,
  see also [timetable](https://events.gnome.org/event/209/timetable/))

Related episodes:

* \#32 GNOME Mobile 2023 Hackfest Special

<small>Editing by: @ollieparanoid,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
