date: 2021-07-29
title: "#7 v21.06, A-GPS, Megapixels, Bluetooth, gPodder, iPhone 7"
length: "37:48"
timestamps:
  - "00:30 postmarketOS v21.06"
  - "01:28 PinePhone: Modem improvements w/ A-GPS"
  - "03:52 Megapixels"
  - "13:40 Bluetooth improvements"
  - "15:15 Various small improvements"
  - "19:08 New devices in the Community category"
  - "20:40 Apple iPhone 7/7+"
  - "22:30 Kobo Clara HD"
  - "25:21 gnome-clocks w/ alarm"
  - "26:50 postmarketos-backup"
  - "29:45 gpodder-adaptive / daily driving"
  - "35:07 shout out to PineTalk"
---

So what is Megapixels? Not only that is answered, but we also take a deep dive
into technical details of our favorite camera application. Besides that we
cover the new release, new devices, and a whole bunch of other news.

Featuring @craftyguy, @dylanvanassche, @ollieparanoid, @MartijnBraam (in order
of appearance). Send in your questions and feedback with \#postmarketOSpodcast
on Mastodon - it's not like we don't have enough content without questions...
but if you submit a good one we might squeeze it into a future episode!

Referenced in this episode:

* [postmarketOS Release: v21.06](https://postmarketos.org/blog/2021/07/04/v21.06-release/)
* PinePhone modem improvements
    * eg25-manager: GNSS assistance support, sync with modem-power, etc.
      ([pma!2185](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2185))
    * modemmanager: add quick suspend/resume patches
      ([pma!2187](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2187))
* Megapixels
    * [repo](https://git.sr.ht/~martijnbraam/megapixels)
    * [issues](https://todo.sr.ht/~martijnbraam/Megapixels)
* Bluetooth OBEX and advertisement
  ([pma!2245](Bluetooth OBEX and advertisement))
* Various small improvements
    * postmarketos-mkinitfs: don't depend on osk-sdl, add to initfs conditionally
      ([pma!2242](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2242))
    * postmarketos-base: move UI-specific config/dependencies into postmarketos-base-ui
      ([pma!2243](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2243))
    * pmbootstrap kconfig improvements
      ([search](https://gitlab.com/postmarketOS/pmbootstrap/-/merge_requests?scope=all&state=merged&search=kconfig))
* New devices in community
    * [device categories](https://wiki.postmarketos.org/wiki/Device_categorization)
    * [OnePlus 6](https://wiki.postmarketos.org/wiki/OnePlus_6_(oneplus-enchilada))
      and [6T](https://wiki.postmarketos.org/wiki/OnePlus_6T_(oneplus-fajita))
      moved to community
      ([pma!2088](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2088))
    * [Xiaomi Mi Note 2](https://wiki.postmarketos.org/wiki/Xiaomi_Mi_Note_2_(xiaomi-scorpio))
      moved to community
      ([pma!2188](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2188))
* Apple iPhone 7/7+
    * [device page](https://wiki.postmarketos.org/wiki/Apple_iPhone_7/7%2B_(apple-iphone7))
    * [checkra1n](https://checkra.in/)
    * [project sandcastle](https://projectsandcastle.org/)
    * [Onny's blog post](https://blog.project-insanity.org/2020/04/22/linux-with-wayland-is-now-running-on-iphone-7/)
    * [pma!2289](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2289)
* Kobo Clara HD
    * [device page](https://wiki.postmarketos.org/wiki/Kobo_Clara_HD_(kobo-clara))
    * [pma!2334](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2334)
    * [Martijn's video](https://spacepub.space/videos/watch/3aac1314-291f-425c-b36a-0f5b6a54e357)
    * [Sxmo after some colorscheme tweaking](https://fosstodon.org/@martijnbraam/106570501974997160)
* gnome-clocks w/ alarm
    * [initial MR](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2342)
    * [plan with further fixes / upstreaming](https://gitlab.com/postmarketOS/pmaports/-/issues/1170)
* postmarketos-backup
    * [repo](https://gitlab.com/postmarketOS/postmarketos-backup)
    * [pma!2261](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2261)
* gpodder-adaptive (ollie managed to fix the two bugs mentioned in the
  episode!)
    * gtkui: fix loading of cached thumbnails ([gpodder!1105](https://github.com/gpodder/gpodder/pull/1105))
    * gtkui: properly scale cover/pill on high resolution displays ([gpodder!1106](https://github.com/gpodder/gpodder/pull/1106))
    * and while at it:
      [gpodder!1107](https://github.com/gpodder/gpodder/pull/1107),
      [gpodder!1108](https://github.com/gpodder/gpodder/pull/1108)
* PineTalk
    * [homepage / web player](https://www.pine64.org/pinetalk/)
    * [the proper opus feed](https://www.pine64.org/feed/opus/) to add to your
      podcatcher
    * episode 13 is the season 1 finale
    * episode 10 is the interview with Martijn

<small>Editing by: @craftyguy,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
