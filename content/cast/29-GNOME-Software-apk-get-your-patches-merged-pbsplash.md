date: 2023-03-17
title: "#29 GNOME Software + apk, get your patches merged, pbsplash"
length: "49:43"
timestamps:
  - "00:21 New Trusted Contributor"
  - "03:47 GNOME Software + apk"
  - "10:12 G-S & Flatpak"
  - "16:20 help wanted"
  - "18:17 G-S experience today"
  - "23:50 apk upgrade -a?"
  - "26:33 patches"
  - "38:50 pbsplash"
---
We are happy to announce that Pablo joined the Trusted Contributors! He has
been a driving force in getting GNOME Software working properly on Alpine and
postmarketOS, and since he is joining us today, guess what we talk about in
this episode! Besides that we get into getting your patches merged and the new
postmarketOS splash screen that just landed in edge.

Featuring @ollieparanoid, @pabloyoyoista, @MartijnBraam, @calebccff (in order
of appearance).

Referenced in this episode:

* New trusted contributor: @pabloyoyoista
    * [Team members](https://wiki.postmarketos.org/wiki/Team_members)

* [GNOME Software](https://gitlab.gnome.org/GNOME/gnome-software) and apk
    * Pablo just wrote a big blog post about the experience:
        * [GNOME Software and postmarketOS: a bumpy road](https://blogs.gnome.org/pabloyoyoista/2023/03/05/gs-and-pmos-a-bumpy-road/)
    * [gnome-software-plugin-apk](https://github.com/Cogitri/gnome-software-plugin-apk)
    * [apk-polkit-rs](https://gitlab.alpinelinux.org/Cogitri/apk-polkit-rs)
    * [alpine-appstream-downloader](https://gitlab.com/pabloyoyoista/alpine-appstream-downloader)
    * [pmbootstrap: install: make cached remote repositories available on first install](https://lists.sr.ht/~postmarketos/pmbootstrap-devel/patches/39354)
    * [pmbootstrap: install: run alpine-appstream-downloader if available](https://lists.sr.ht/~postmarketos/pmbootstrap-devel/patches/39351)
    * Georges Stavracas: [Profiling & optimizing GNOME Software](https://feaneron.com/2023/02/07/profiling-optimizing-gnome-software/)

* Help wanted
    * [gnome-software-plugin-apk#69](https://github.com/Cogitri/gnome-software-plugin-apk/issues/69)
      about porting to GNOME 44
    * [apk-polkit-rs#15](https://gitlab.alpinelinux.org/Cogitri/apk-polkit-rs/-/issues/15)
      about downloading packages before running upgrades
    * [Other](https://github.com/Cogitri/gnome-software-plugin-apk/issues)
      [issues](https://gitlab.alpinelinux.org/Cogitri/apk-polkit-rs/-/issues)

* Get your patches merged
    * COMMITSTYLE.md: [Alpine](https://gitlab.alpinelinux.org/alpine/aports/-/blob/master/COMMITSTYLE.md),
      [postmarketOS](https://gitlab.com/postmarketOS/pmaports/-/blob/master/COMMITSTYLE.md)
    * Alpine's [.githooks](https://gitlab.alpinelinux.org/alpine/aports/-/tree/master/.githooks)

* pbsplash
    * [git repo](https://git.sr.ht/~calebccff/pbsplash)
    * [demo video](https://fosstodon.org/@postmarketOS/109887055754137919)

<small>Editing by: @MartijnBraam,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
