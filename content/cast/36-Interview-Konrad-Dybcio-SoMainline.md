date: 2023-11-26
title: "#36 INTERVIEW: Konrad Dybcio (of SoMainline Fame)"
length: "1:09:15"
timestamps:
  - "00:00 Pronunciation"
  - "00:29 What is SoMainline"
  - "01:15 Intro"
  - "02:11 Origin story"
  - "15:18 linux-msm"
  - "20:05 Thinkpad X13s"
  - "23:07 What can distros do"
  - "29:12 Community submissions"
  - "34:02 Best/worst QC phones for mainline"
  - "36:00 Reduce the number of QC kernel forks"
  - "44:40 Linux on A7-A8X"
  - "51:00 Integrating Apples"
  - "60:51 Questions for us"
  - "66:59 Shout out"
---
"Why am I running Linux on my desktop, but not on my phone?" is what Konrad
asked himself at the beginning of this amazing story that ends up with him
becoming a maintainer for Qualcomm support in Linux.

Featuring @ollieparanoid, @konradybcio, @craftyguy, @calebccff (in order of
appearance).

[Konrad accepts donations, preferrably in form of tea ☕.](https://konradybcio.pl/donate/)

Referenced in this episode:

* [SoMainline](https://somainline.org/)
* B4 ("before") - tool for dealing with kernel patches
    * [docs](https://b4.docs.kernel.org/en/latest/)
    * [source](https://git.kernel.org/pub/scm/utils/b4/b4.git)
* [linux-arm-msm mailing list](https://patchwork.kernel.org/project/linux-arm-msm/list/)
* [Asahi Linux](https://asahilinux.org/)
    * [speakersafetyd](https://github.com/asahilinux/speakersafetyd)
* {{issue|1922|pmaports}}: Feature request: 'devbox setup'
    * The issue Konrad created about having a minimal image for doing kernel
      development. Reply there if interested, contributions welcome!
* {{MR|4420|pmaports}}: Add initial on-device CI support
    * Caleb's new initramfs hook for running the postmarketOS initramfs in a
      testing environment (QEMU, CI, or manual testing).
* [Linux on A7-A8X](https://konradybcio.pl/linuxona7/)
* [checkra1n](https://checkra.in/)
* [Project Sandcastle](https://projectsandcastle.org/)

Channels:

* IRC: [#linux-msm](irc://irc.oftc.net/#linux-msm) on irc.oftc.net
* Matrix: [#mainline:postmarketos.org](https://matrix.to/#/#mainline:postmarketos.org)


Related episodes:

* \#08 INTERVIEW: Caleb Connolly (of OnePlus 6 / SDM845 Mainlining Fame)

<small>Editing by: @ollieparanoid,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
