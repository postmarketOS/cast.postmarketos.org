date: 2023-06-24
title: "#32 GNOME Mobile 2023 Hackfest Special"
length: "38:16"
timestamps:
  - "00:00 Intro"
  - "00:44 All about GNOME?"
  - "02:58 Reddit blackout"
  - "07:10 Is it going"
  - "07:55 Julian"
  - "09:16 Tobias"
  - "12:20 Jonas"
  - "13:28 Luca"
  - "15:02 Caleb"
  - "18:09 Jane"
  - "20:10 Oliver"
  - "23:00 Portals"
  - "23:50 Do-whatever-day"
  - "28:53 Lockscreen app support"
  - "34:34 On that note"
---
Featuring @ollieparanoid, @MartijnBraam, @calebccff, @jsparber, @tbernard,
@verdre, @z3ntu, @jane (in order of appearance).

Upcoming events (close dates, there are many more):

* July 13-16 — Portland, OR: [FOSSY 2023](https://2023.fossy.us/)
    * @anjan from [Sxmo](https://sxmo.org) will hold a talk:
  [Thoughts after daily driving postmarketOS for 3 years](https://2023.fossy.us/schedule/presentation/81/)
* July 15-21 — Thessaloniki, Greece: [Akademy 2023](https://akademy.kde.org/2023/)
* July 26-31 — Riga, Latvia: [GUADEC 2023](https://events.gnome.org/event/101/)

Threadiverse:

* [Update from Lemmy after the Reddit blackout](https://join-lemmy.org/news/2023-06-17_-_Update_from_Lemmy_after_the_Reddit_blackout)
* [How to find /c/postmarketOS on Lemmy and kbin](https://fosstodon.org/@postmarketOS/110566848301304065)
* [https://kbin.social/m/RedditMigration](https://kbin.social/m/RedditMigration)

GNOME Hackfest:

* [v23.06: From the GNOME Mobile 2023 Hackfest](https://postmarketos.org/blog/2023/06/07/v23.06-release/)
* [Berlin Mobile Hackfest](https://blogs.gnome.org/tbernard/2023/06/16/berlin-mobile-hackfest/)
  (great blog post with a
   [photo](https://blogs.gnome.org/tbernard/files/2023/06/photo_7_2023-06-16_16-35-00-768x576.jpg)
   of recording this episode)
* [postmarketOS on the LG G Watch R](https://floss.social/@sonny/110497716117529138)
* [GNOME Shell running out of the box on postmarketOS 23.06](https://floss.social/@sonny/110503742154366509)
* [Video call on Librem 5](https://floss.social/@rmader/110544281442771371)
* [Stickers](https://floss.social/@sonny/110485707595358509)
* [Behold the power of mainline Linux phones!](https://mastodon.social/@tbernard/110498370733523299)
* [Consider taking over some GNOME Mobile MRs](https://mastodon.social/@tbernard/110497694038863336)

<small>Editing by: @ollieparanoid,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
