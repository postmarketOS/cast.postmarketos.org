date: 2022-05-02
title: "#17 INTERVIEW: Peter Mack (of LinuxPhoneApps, LINMOB.net Fame)"
length: "54:20"
timestamps:
  - "00:50 LinuxPhoneApps"
  - "01:20 App Testing"
  - "04:02 Comments"
  - "08:12 LINMOB.net"
  - "13:22 2022 Changes"
  - "15:50 Other Editors"
  - "18:30 Dropping Numbers"
  - "19:38 Videos"
  - "23:50 PineTalk origin story"
  - "26:34 TechnikTechnik"
  - "29:20 Future plans"
  - "31:30 App packaging"
  - "35:20 OS/devices you use"
  - "46:17 Electron"
  - "51:55 Sxmo talk"
  - "53:00 Outro"

---
If you are like us and open the virtual LINMOB.net newspaper once a week to
discover what's new in the scene without getting your brain fried by social
media, you'll be as excited as we are to learn more about the person that keeps
the show running. The weekly updates is just one of his endeavours of course,
we also talk about LinuxPhoneApps and some other cool projects he has been
involved with.

Featuring @ollieparanoid, @MartijnBraam, @calebccff, @PureTryOut, @linmob (in
order of appearance).

Referenced in this episode:

* [LinuxPhoneApps](https://linuxphoneapps.org)
    * [Hello World](https://linuxphoneapps.org/blog/its-better-to-launch/) blog
      post
    * Atom feed for [new apps](https://linuxphoneapps.org/apps/atom.xml)

* [LINMOB.net](https://linmob.net/)
    * Weekly Update (1-2/2022) Editorial:
      [A new start, a slightly different format](https://linmob.net/weekly-update-1-2-2022/)
    * [Videos](https://linmob.net/videos/)

* Podcasts
    * [PineTalk](https://www.pine64.org/pinetalk/) Season 1
    * [TechnikTechnik](https://techniktechnik.de/)

* OS/devices
    * [Asahi Linux](https://asahilinux.org/)
    * Puri.sm blog: [Adventures of porting postmarketOS to the Librem 5](https://puri.sm/posts/adventures-of-porting-postmarketos-to-the-librem-5/)
    * [Xiaomi Poco F1](https://wiki.postmarketos.org/wiki/Xiaomi_Poco_F1_(xiaomi-beryllium)),
      call audio support [pma!2982](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2982)
    * Maemo Leste w/ [Motorola Droid 4](https://leste.maemo.org/Motorola_Droid_4)

* How to contribute
    * If you would like to write for LINMOB.net,
      [get in touch](https://linmob.net/about/)!
    * LinuxPhoneApps:
      [adding or editing app listings](https://linuxphoneapps.org/docs/help/faq/#adding-or-editing-app-listings)
    * Found something outdated on LinuxPhoneApps/LINMOB.net? Submit a patch (or
      at least write to Peter).
    * Package/maintain your favorite mobile apps for your favorite distros.
    * Don't rant about bugs on social media, help to get them fixed instead.

* Talk recommendation
    * LibrePlanet: [Sxmo: Freedom on mobile devices through simplicity and hackability](https://media.libreplanet.org/u/libreplanet/m/sxmo-freedom-on-mobile-devices-through-simplicity-and-hackability/)

<small>Editing by: @MartijnBraam, @ollieparanoid,
Music by: [The Passion HiFi](http://www.thepassionhifi.com),
[Joe In Here](https://soundcloud.com/ubports/lets-bring-back-the-touch)</small>
