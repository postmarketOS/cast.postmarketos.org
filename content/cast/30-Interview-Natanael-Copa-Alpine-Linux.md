date: 2023-04-23
title: "#30 INTERVIEW: Natanael Copa (of Alpine Linux Fame)"
length: "1:12:58"
timestamps:
  - "00:09 Upcoming event"
  - "01:00 The hellos"
  - "01:14 Choosing Alpine as name"
  - "03:13 Alpine origin story"
  - "15:17 apk-tools v2"
  - "26:40 apk-tools v3"
  - "33:00 Alpine becoming great"
  - "39:17 Radical changes in the stack"
  - "44:00 Reproducible builds"
  - "50:37 Different goals"
  - "55:27 If you were not working on Alpine / bus factor"
  - "59:34 Questions for us"
  - "62:18 pmOS build infra"
  - "64:55 CPU architectures"
  - "68:30 Shout out"
  - "72:00 Outro"
---
Natanael Copa joins us to talk about Alpine's story. From being dead scared to
fork the initial version off from Gentoo to Alpine becoming bigger than himself
and making sure it stays around with a good bus factor. If you ever wondered
how exactly apk is so incredibly fast, that is also covered - and what plans
there are to make it even faster in the future. Also what would be necessary to
get reproducible builds in Alpine and more. Enjoy!

Featuring @ollieparanoid, @calebccff, @MartijnBraam, @pabloyoyoista, @ncopa (in
order of appearance).

Referenced in this episode:

* [Alpine Linux](https://alpinelinux.org/)
* [apk-tools](https://sourceforge.net/projects/apk-tools/) v1 on sourceforge:
  "A shell based package manager for embedded *nix systems similar FreeBSD pkgtools."
* [Reproducible Builds — a set of software development practices that create an independently-verifiable path from source to binary code](https://reproducible-builds.org/)
* [Cross compiling in pmbootstrap](https://wiki.postmarketos.org/wiki/Build_internals#Cross-compile_types)
    * [crossdirect](https://gitlab.com/postmarketOS/pmaports/-/blob/85833295511cca34265c4b7c1357371204757847/cross/crossdirect/APKBUILD) (thanks to @zhuowei for contributing the initial version!)
    * previously using distcc, inspired by [alarm](https://archlinuxarm.org/wiki/Distcc_Cross-Compiling)
* [k0s | Kubernetes distribution for bare-metal, on-prem, edge, IoT](https://k0sproject.io/)

Upcoming event on Saturday, April 29th:

* [Augsburger Linux-Infotag 2023](https://linmob.net/see-you-in-augsburg-lit-2023/)

<small>Editing by: @ollieparanoid,
Music by: [The Passion HiFi](http://www.thepassionhifi.com), @ollieparanoid
</small>
