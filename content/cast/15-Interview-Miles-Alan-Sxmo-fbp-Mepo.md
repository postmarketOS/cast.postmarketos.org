date: 2022-02-26
title: "#15 INTERVIEW: Miles Alan (of Sxmo, fbp, Mepo Fame)"
length: "50:48"
timestamps:
  - "01:00 Sxmo Early Days"
  - "10:31 Framebufferphone"
  - "15:53 Zig/Oil"
  - "19:59 Mepo"
  - "32:40 What OS/UI Do You Use"
  - "36:27 Music"
  - "37:23 Wishes/Future"
  - "46:50 Shout Out"
  - "47:40 How To Contribute"

---
We got a chance to talk to Miles Alan about a lot of his exciting projects:
how he started Sxmo, his new and even more minimalistic framebufferphone UI,
and the super fast map application Mepo. The latter will not only run on Sxmo
and fbp, but also Phosh and possibly on Plasma Mobile and more UIs.

Featuring @craftyguy, @ollieparanoid, @MartijnBraam, @mil (in order of
appearance).

Referenced in this episode:

* Sxmo Early Days
    * [suckless.org](https://suckless.org/)
    * [Sxmo 0.1 thread on PINE64 forums](https://forum.pine64.org/showthread.php?tid=9913)
      (2020-05-20)

* Framebufferphone
    * [Sourcehut project](https://sr.ht/~mil/framebufferphone/) with a
      nice high-level introduction (as with all of Miles' Sourcehut projects
      linked below)
    * [User guide](https://git.sr.ht/~mil/fbp#strongfbpstrong) describing the
      modes/keybindings/fifo API and how to use the UI

* Zig & Oil Shell
    * [ziglang.org](https://ziglang.org/)
    * [oilshell.org](https://www.oilshell.org/)

* Mepo
    * [Sourcehut project](https://sr.ht/~mil/Mepo/)

* Wishes/Future
    * Alpine's [diskless mode](https://wiki.alpinelinux.org/wiki/Installation#Diskless_Mode):
      installs all packages into RAM,
      [local backup](https://wiki.alpinelinux.org/wiki/Alpine_local_backup)
      is used to store state
    * [NetSurf](https://www.netsurf-browser.org/),
      [visurf](https://sr.ht/~sircmpwn/visurf/)
    * FOSDEM2022: [Linux Mobile vs. The Social Dilemma](https://fosdem.org/2022/schedule/event/mobile_social_dilemma/)

* How To Contribute
    *  Miles projects, links to mailing lists etc are on
       [Sourcehut](https://sr.ht/~mil/)

<small>Editing by: @MartijnBraam,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
