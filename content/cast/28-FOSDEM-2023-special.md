date: 2023-02-12
title: "#28 FOSDEM 2023 Special"
length: "30:28"
timestamps:
  - "00:00 Steal your mic"
---
Between the Saturday closing of FOSDEM 2023 and going to a restaurant, we
managed to squeeze around a table outside the cafeteria to record the very
first ever in-person postmarketOS podcast episode! Fresh in the moment we share
our experiences from the FOSS on mobile devroom, meeting great people at the
Linux on Mobile stand (and everywhere else at FOSDEM/in Brussels) and random
other cool things that were going down.

Featuring @spaetz (sampled in the intro), @ollieparanoid, @linmob, @craftyguy,
@MartijnBraam, @calebccff, @PureTryOut, @z3ntu (in order of appearance).

Photos:

* @calebccff: ["recorded the first in person @postmarketOS podcast episode LIVE from #FOSDEM !"](https://fosstodon.org/@calebccff/109808038695004886)
* @craftyguy: ["#postmarketOS core team... Assemble!"](https://freeradical.zone/@craftyguy/109807524847524784)
* @awai: ["The beauty and diversity of the #LinuxOnMobile bazaar!"](https://fosstodon.org/@awai/109812598426346120)
* @z3ntu: ["The Linux on Mobile stand at #FOSDEM is pretty packed!"](https://fosstodon.org/@z3ntu/109812602670785848)
* @linmob: ["#FOSDEM was really awesome, it only went by too quickly!…"](https://fosstodon.org/@linmob/109814654331788517)
* @mntmn: ["this was ~intense~! thanks to everyone who was interested in MNT Pocket Reform…"](https://mastodon.social/@mntmn/109808024287105873)

Video recordings of the talks:

* [Where do we go from here? (Audio fixup)](https://spacepub.space/w/4caM2SQS2we81hyRuPrFPY)
    * Matrix room: [#fossmo-wg:matrix.org](https://matrix.to/#/#fossmo-wg:matrix.org)
* [FOSS on mobile devices devroom](https://fosdem.org/2023/schedule/track/foss_on_mobile_devices/) (as of writing, most talks have already been uploaded)

PinePhone and Lima / Mesa:

We're aware that
[mesa#8198](https://gitlab.freedesktop.org/mesa/mesa/-/issues/8198)
is still causing problems for people on the PinePhone. Big thanks to @enues
from mesa who was talking to us at the stand and is now looking into it! We
also have issue
[pmaports#805](https://gitlab.com/postmarketOS/pmaports/-/issues/805) about
screen freezes. If you are affected by this and would like to help out, firing
up a profiler and making/contributing to detailed, useful bugreports upstream
at mesa is appreciated.

@spaetz from Mobian did a great job at moderating the devroom! (Correction:
Ollie said from Maemo instead of Mobian in the recording.)

<small>Editing by: @ollieparanoid,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
