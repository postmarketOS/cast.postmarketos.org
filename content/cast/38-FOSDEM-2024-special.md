date: 2024-02-14
title: "#38 FOSDEM 2024 Special"
length: "31:19"
timestamps:
  - "00:01 Shall we start"
---
Compared to last year, we decided to do the recording *inside* the cafeteria
instead of *outside*, and on *Sunday* instead of *Saturday*. Twice the amount
of FOSDEM days to talk about, and with more people talking in the background.
Enjoy!

Featuring @z3ntu, @ollieparanoid, @pabloyoyoista, @craftyguy, @Newbyte, @dos,
@PureTryOut, @linmob (in order of appearance), also @~r (sampled in the outro).

**Not included in the episode:** a three day post-FOSDEM hackathon, read all about it
[on the blog](https://postmarketos.org/blog/2024/02/14/fosdem-and-hackathon/)!

Video recordings of the talks:

* [FOSS on Mobile Devices devroom](https://fosdem.org/2024/schedule/track/foss-on-mobile-devices/)
* [Open Source for Sustainable and Long lasting Phones](https://fosdem.org/2024/schedule/event/fosdem-2024-3362-open-source-for-sustainable-and-long-lasting-phones/) (the Fairphone talk!)

Related episodes:

* \#28 FOSDEM 2023 special
* \#14 FOSDEM 2022 special

<small>Editing by: @ollieparanoid,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
