date: 2024-06-23
title: "#41 INTERVIEW: Guido Günther (of Phosh, Librem 5 Fame)"
length: "49:37"
timestamps:
  - "00:00 avoiding android"
  - "01:04 agx"
  - "01:41 origin story"
  - "06:06 relationship phosh & purism"
  - "07:02 pinephone"
  - "09:50 lomiri"
  - "11:11 squeekboard vs phosh-osk-stub"
  - "16:24 devices"
  - "20:03 greatness"
  - "23:42 widgets"
  - "28:12 testing"
  - "33:55 froscon"
  - "36:40 cellbroadcast"
  - "39:40 how 2 contrib"
  - "41:48 future plans"
  - "45:18 shout out"
  - "45:50 donating to phosh development"
  - "46:20 wrapping up"
---
Guido is the main Phosh developer. He tells us how getting involved with Purism
was just a happy coincidence, how he went straight from the N900 to the L5 and
a lot more in this exciting interview!

Featuring @ollieparanoid, @agx and @Newbyte (in order of appearance).

[Phosh accepts donations via Liberapay.](https://liberapay.com/Phosh/)

Related links:

* [Guido's OpenMoko setup](https://honk.sigxcpu.org/projects/swirlberry/screenshots/tmut-folder.png)
* [Phosh](https://phosh.mobi)
    * Contributing
        * [What's a good way to contribute?](https://phosh.mobi/faq/#whats-a-good-way-to-contribute)
        * [Matrix room](https://matrix.to/#/#phosh:talk.puri.sm)
        * [Translations](https://l10n.gnome.org/module/phosh/)
    * Blog
        * [Build Your Own Quick-Setting or Lock-Screen Widget](https://phosh.mobi/posts/custom-plugins-dev/)
        * [Don't unblank in my back pack please](https://phosh.mobi/posts/wakeup-keys/)
    * [phosh-osk-stub](https://gitlab.gnome.org/guidog/phosh-osk-stub) &
      [squeekboard](https://gitlab.gnome.org/World/Phosh/squeekboard)
        * [pmaports#2628](https://gitlab.com/postmarketOS/pmaports/-/issues/2628):
          Switch Phosh from Squeekboard to phosh-osk-stub
        * [squeekboard#244](https://gitlab.gnome.org/World/Phosh/squeekboard/-/issues/244):
          Make sure Electron applications receive correct input
    * [phosh!1079](https://gitlab.gnome.org/World/Phosh/phosh/-/merge_requests/1079):
      Split the app grid out from the overview
* Notches
    * [gmobile](https://gitlab.gnome.org/World/Phosh/gmobile)
    * [Display notch support](https://lore.kernel.org/all/c0cdabab-74b6-b2e6-4a4d-edd737d5ae6a@connolly.tech/T/)
    &mdash; *Caleb's LKML post*
* [FrOSCon](https://froscon.org/)
    * [Our request for a #FossOnMobile devroom at #FrOSCon24 (17. / 18. of August) just got accepted 🚀 .If you want to give a talk, do a BoF, round table or similar…](https://social.librem.one/@agx/112590703971553761)
    * [Matrix room](https://matrix.to/#/#froscon-linux-mobile:matrix.org)
* [Bananui](https://wiki.postmarketos.org/wiki/Bananui)
* [Lomiri on postmarketOS](https://wiki.postmarketos.org/wiki/Lomiri)
* [UnifiedPush](https://unifiedpush.org/)
* [Adventures of porting postmarketOS to the Librem 5](https://puri.sm/posts/adventures-of-porting-postmarketos-to-the-librem-5/)
  &mdash; *Clayton's blog post from 2020*

Related episodes:

* \#35 INTERVIEW: a-wai, devrtz, kop316 (of Mobian Fame)
* \#27 Chromebooks, … &mdash; *Anton talks about pmOS on Chromebooks*
* \#21 INTERVIEW: Sebastian Krzyszkowiak (of Phosh, Librem 5 Fame)

<small>Editing by: @ollieparanoid,
Music by: [The Passion HiFi](http://www.thepassionhifi.com)</small>
