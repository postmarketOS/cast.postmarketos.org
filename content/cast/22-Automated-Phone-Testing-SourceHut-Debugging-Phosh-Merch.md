date: 2022-09-12
title: "#22 Automated Phone Testing, SourceHut, Debugging Phosh, Merch"
length: "44:18"
timestamps:
  - "02:14 Welcome"
  - "02:29 Test setup"
  - "10:18 Considering SourceHut"
  - "17:11 Bluetooth / Pulseaudio / ModemManager"
  - "19:37 SHIFT6mq"
  - "22:58 Fun with Phosh"
  - "32:58 GTK scaling and gestures"
  - "35:13 Merch"
  - "42:50 Outro"
---
In this episode we talk about the difficulties of automated phone testing. Have
a follow up on the SourceHut consideration and do a deep dive into both Phosh
debugging and Bluetooth features. Also there are t-shirts now! Have we
mentioned the t-shirts?

Featuring @ollieparanoid, @calebccff, @MartijnBraam (in order of
appearance).

Referenced in this episode:

* [Ardour](https://ardour.org/)

* [Automated Phone Testing pt.1](https://blog.brixit.nl/automated-phone-testing-pt-1/)

* [Considering SourceHut, Part 2](https://postmarketos.org/blog/2022/08/26/considering-sourcehut-2/)
    * pmbootstrap check idea ([pmb#2169](https://gitlab.com/postmarketOS/pmbootstrap/-/issues/2169))

* Bluetooth / Pulseaudio / ModemManager
    * bluetooth: report AG battery level
      ([pulseaudio!631](https://gitlab.freedesktop.org/pulseaudio/pulseaudio/-/merge_requests/631))
    * Add DTMF support for QMI modems
      ([ModemManager!823](https://gitlab.freedesktop.org/mobile-broadband/ModemManager/-/merge_requests/823))

* plugins: android-boot: new plugin
  ([fwupd!4993](https://github.com/fwupd/fwupd/pull/4993))

* Debugging Phosh
    * "error in client communication" on samsung-m0
      ([phosh#828](https://gitlab.gnome.org/World/Phosh/phosh/-/issues/828))
    * New wiki articles:
        * [Debugging the GNOME stack](https://wiki.postmarketos.org/wiki/Debugging_the_GNOME_stack)
        * [Analyzing Coredumps](https://wiki.postmarketos.org/wiki/Analyzing_Coredumps)
    * [v22.06 SP2: The One That Swipes](https://postmarketos.org/blog/2022/09/04/v22.06.2-release/)
    * Scaling introduces stepping to gestures and GTK applications
      ([phoc#293](https://gitlab.gnome.org/World/Phosh/phoc/-/issues/293))

* Merch
    * [https://postmarketos.org/merch](https://postmarketos.org/merch)
    * [artwork.git](https://gitlab.com/postmarketOS/artwork/)

<small>Editing by: @MartijnBraam,
Music by: [The Passion HiFi](http://www.thepassionhifi.com),
@ollieparanoid</small>
